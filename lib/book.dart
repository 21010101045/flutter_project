import 'package:flutter/material.dart';

class Book extends StatelessWidget {
  const Book({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
      ),
      body: Center(
          child: Text(
            "Tickets Booked \n Thank You",
            style: TextStyle(color: Colors.black, fontSize: 40),
          )),
    );
  }
}

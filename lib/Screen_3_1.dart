import 'package:flutter/material.dart';

import 'book.dart';

class Screen_3_1 extends StatelessWidget {
  const Screen_3_1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            flex: 2,
            child: Stack(
              children: [
                Container(
                  width: double.infinity,
                  height: double.infinity,
                  child: Image.asset(
                    'assets/images/Nusa Penida.jpg',
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.symmetric(vertical: 50, horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                              ),
                              child: Icon(
                                Icons.chevron_left,
                                color: Colors.black,
                                size: double.tryParse('25'),
                              )),
                        ),
                        Icon(
                          Icons.more_horiz,
                          color: Colors.white,
                          size: double.tryParse("40"),
                        ),
                      ],
                    )),
                Container(
                  alignment: Alignment.bottomRight,
                  margin:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  child: CircleAvatar(
                    backgroundColor: Colors.white.withOpacity(0.4),
                    child: Transform.scale(
                      scaleY: -1,
                      child: const Icon(
                        Icons.open_in_full_sharp,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20, top: 25, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Nusa Penida',
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold)),
                      Icon(Icons.favorite, color: Colors.redAccent),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(left: 20, top: 15),
                  child: const Text(
                    'Bali, Indonesia •\$ 329',
                    style: TextStyle(fontSize: 19),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          CircleAvatar(
                            backgroundColor: Colors.white,
                            child: Icon(Icons.star, color: Colors.amber),
                          ),
                          SizedBox(height: 5),
                          Text(
                            '4.8',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 5),
                          Text(
                            '2k review',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 15,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          CircleAvatar(
                            backgroundColor: Colors.white,
                            child:
                                Icon(Icons.route_sharp, color: Colors.indigo),
                          ),
                          SizedBox(height: 5),
                          Text(
                            '7km',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'Direction',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 15,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          CircleAvatar(
                            backgroundColor: Colors.white,
                            child: Icon(Icons.sunny, color: Colors.redAccent),
                          ),
                          SizedBox(height: 5),
                          Text(
                            '20°C',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'Sunny',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 15,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(top: 20, left: 20),
                  child: const Text(
                    'Description',
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),
                ),
                Container(
                  height: 70,
                  margin: const EdgeInsets.only(top: 20, left: 20),
                  child: const SingleChildScrollView(
                    child: Text(
                      "Nusa Penida is an island southeast of Indonesia's island Bali and a district of Klungkung Regency that includes the neighbouring small island of Nusa Lembongan. The Badung Strait separates the island and Bali. The interior of Nusa Penida is hilly with a maximum altitude of 524 metres.",
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 150,
                  height: 50,
                  margin: EdgeInsets.only(top: 5),
                  child: InkWell(
                    onTap: () {
                      print("Tickets Booked");
                      print("Thank You");
                    },
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) {
                            return Book();
                          },
                        ));
                      },
                      child: Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: Colors.deepOrange,
                            borderRadius: BorderRadius.circular(45)),
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Text(
                            'Book Now',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 19,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

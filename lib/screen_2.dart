import 'package:flutter/material.dart';
import 'demo.dart';
import 'drawer.dart';
import 'screen_3.dart';

class Screen2 extends StatefulWidget {
  const Screen2({Key? key}) : super(key: key);

  @override
  State<Screen2> createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {
  bool isClicked = true;
  Color mycolor = Colors.black;
  bool isTap = true;
  Icon myIcon = Icon(Icons.favorite_border_rounded);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 35, left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) {
                            return Drawer1(child: ListView());
                          },
                        ));
                      },
                      icon: Icon(
                        Icons.menu,
                        color: Colors.black,
                        size: 30,
                      )),
                  CircleAvatar(
                    backgroundImage: AssetImage('assets/images/pngegg.png'),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
              alignment: Alignment.topLeft,
              child: Text(
                "Let's enjoy",
                style: TextStyle(
                  fontSize: 25,
                  fontFamily: 'RobotoSlab',
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 25),
              alignment: Alignment.topLeft,
              child: Text(
                "Your Vacation!",
                style: TextStyle(
                    fontFamily: 'RobotoSlab',
                    fontSize: 30,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: Colors.white),
              ),
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Search Your Trip",
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.black,
                  ),
                  suffixIcon: CircleAvatar(
                      child: Image.asset(
                    'assets/images/filter-icon.png',
                    color: Colors.white,
                    height: 20,
                  )),
                  border: InputBorder.none,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      if (isClicked) {
                        setState(() {
                          mycolor = Colors.cyan;
                          isClicked = !isClicked;
                        });
                      } else {
                        setState(() {
                          mycolor = Colors.black;
                          isClicked = !isClicked;
                        });
                      }
                    },
                    child: Text(
                      'All',
                      style: TextStyle(
                          fontFamily: 'RobotoSlab',
                          color: mycolor,
                          fontSize: 15),
                    ),
                  ),
                  Text(
                    'New',
                    style: TextStyle(
                        fontFamily: 'RobotoSlab',
                        color: Colors.black,
                        fontSize: 15),
                  ),
                  Text(
                    'Most Viewed',
                    style: TextStyle(
                        fontFamily: 'RobotoSlab',
                        color: Colors.black,
                        fontSize: 15),
                  ),
                  Text(
                    'Recommended',
                    style: TextStyle(
                        fontFamily: 'RobotoSlab',
                        color: Colors.black,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
            Container(
              height: 250,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return Screen3();
                        },
                      ));
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(25),
                            child: Image.asset('assets/images/Nusa Penida.jpg',
                                fit: BoxFit.fill, width: 200, height: 300),
                          ),
                          Container(
                            width: 180,
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              children: [
                                InkWell(
                                  onTap: () {
                                    if (isTap) {
                                      setState(() {
                                        myIcon = Icon(Icons.favorite_border);
                                        isTap = !isTap;
                                      });
                                    } else {
                                      isTap = !isTap;
                                      setState(() {
                                        myIcon = Icon(
                                          Icons.favorite_rounded,
                                          color: Colors.red,
                                        );
                                      });
                                    }
                                  },
                                  child: Container(
                                    alignment: Alignment.topRight,
                                    margin: EdgeInsets.symmetric(
                                        vertical: 20, horizontal: 5),
                                    child: myIcon,
                                  ),
                                ),
                                Container(
                                  height: 120,
                                  width: 300,
                                  decoration: BoxDecoration(),
                                ),
                                Container(
                                  alignment: Alignment.bottomLeft,
                                  margin: EdgeInsets.only(top: 3, left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Nusa Penida',
                                        style: TextStyle(
                                            fontFamily: 'RobotoSlab',
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        children: [
                                          Icon(Icons.location_on_sharp,
                                              color: Colors.white, size: 15),
                                          Text('Bali, Indonesia',
                                              style: TextStyle(
                                                  fontFamily: 'RobotoSlab',
                                                  color: Colors.white)),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return Demo();
                        },
                      ));
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 20),
                      child: Stack(
                        alignment: Alignment.bottomCenter,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(25),
                            child: Image.asset('assets/images/Raja Ampat.jpg',
                                fit: BoxFit.fill, width: 200, height: 300),
                          ),
                          Container(
                            width: 180,
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              children: [
                                InkWell(
                                  onTap: () {
                                    if (isTap) {
                                      setState(() {
                                        myIcon = Icon(Icons.favorite_border);
                                        isTap = !isTap;
                                      });
                                    } else {
                                      isTap = !isTap;
                                      setState(() {
                                        myIcon = Icon(
                                          Icons.favorite_rounded,
                                          color: Colors.red,
                                        );
                                      });
                                    }
                                  },
                                  child: Container(
                                    alignment: Alignment.topRight,
                                    margin: EdgeInsets.symmetric(
                                        vertical: 20, horizontal: 5),
                                    child: myIcon,
                                  ),
                                ),
                                Container(
                                  height: 120,
                                  width: 300,
                                  decoration: BoxDecoration(),
                                ),
                                Container(
                                  alignment: Alignment.bottomLeft,
                                  margin: EdgeInsets.only(top: 3, left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Raja Ampat',
                                        style: TextStyle(
                                            fontFamily: 'RobotoSlab',
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        children: [
                                          Icon(Icons.location_on_sharp,
                                              color: Colors.white, size: 15),
                                          Text('Lombok, Indonesia',
                                              style: TextStyle(
                                                  fontFamily: 'RobotoSlab',
                                                  color: Colors.white)),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return Screen3();
                        },
                      ));
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(25),
                            child: Image.asset('assets/images/Nusa Penida.jpg',
                                fit: BoxFit.fill, width: 200, height: 300),
                          ),
                          Container(
                            width: 180,
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              children: [
                                InkWell(
                                  onTap: () {
                                    if (isTap) {
                                      setState(() {
                                        myIcon = Icon(Icons.favorite_border);
                                        isTap = !isTap;
                                      });
                                    } else {
                                      isTap = !isTap;
                                      setState(() {
                                        myIcon = Icon(
                                          Icons.favorite_rounded,
                                          color: Colors.red,
                                        );
                                      });
                                    }
                                  },
                                  child: Container(
                                    alignment: Alignment.topRight,
                                    margin: EdgeInsets.symmetric(
                                        vertical: 20, horizontal: 5),
                                    child: myIcon,
                                  ),
                                ),
                                Container(
                                  height: 120,
                                  width: 300,
                                  decoration: BoxDecoration(),
                                ),
                                Container(
                                  alignment: Alignment.bottomLeft,
                                  margin: EdgeInsets.only(top: 3, left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Nusa Penida',
                                        style: TextStyle(
                                            fontFamily: 'RobotoSlab',
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        children: [
                                          Icon(Icons.location_on_sharp,
                                              color: Colors.white, size: 15),
                                          Text('Bali, Indonesia',
                                              style: TextStyle(
                                                  fontFamily: 'RobotoSlab',
                                                  color: Colors.white)),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return Demo();
                        },
                      ));
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 20),
                      child: Stack(
                        alignment: Alignment.bottomCenter,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(25),
                            child: Image.asset('assets/images/Raja Ampat.jpg',
                                fit: BoxFit.fill, width: 200, height: 300),
                          ),
                          Container(
                            width: 180,
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              children: [
                                InkWell(
                                  onTap: () {
                                    if (isTap) {
                                      setState(() {
                                        myIcon = Icon(Icons.favorite_border);
                                        isTap = !isTap;
                                      });
                                    } else {
                                      isTap = !isTap;
                                      setState(() {
                                        myIcon = Icon(
                                          Icons.favorite_rounded,
                                          color: Colors.red,
                                        );
                                      });
                                    }
                                  },
                                  child: Container(
                                    alignment: Alignment.topRight,
                                    margin: EdgeInsets.symmetric(
                                        vertical: 20, horizontal: 5),
                                    child: myIcon,
                                  ),
                                ),
                                Container(
                                  height: 120,
                                  width: 300,
                                  decoration: BoxDecoration(),
                                ),
                                Container(
                                  alignment: Alignment.bottomLeft,
                                  margin: EdgeInsets.only(top: 3, left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Raja Ampat',
                                        style: TextStyle(
                                            fontFamily: 'RobotoSlab',
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        children: [
                                          Icon(Icons.location_on_sharp,
                                              color: Colors.white, size: 15),
                                          Text('Lombok, Indonesia',
                                              style: TextStyle(
                                                  fontFamily: 'RobotoSlab',
                                                  color: Colors.white)),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return Screen3();
                        },
                      ));
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(25),
                            child: Image.asset('assets/images/Nusa Penida.jpg',
                                fit: BoxFit.fill, width: 200, height: 300),
                          ),
                          Container(
                            width: 180,
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              children: [
                                InkWell(
                                  onTap: () {
                                    if (isTap) {
                                      setState(() {
                                        myIcon = Icon(Icons.favorite_border);
                                        isTap = !isTap;
                                      });
                                    } else {
                                      isTap = !isTap;
                                      setState(() {
                                        myIcon = Icon(
                                          Icons.favorite_rounded,
                                          color: Colors.red,
                                        );
                                      });
                                    }
                                  },
                                  child: Container(
                                    alignment: Alignment.topRight,
                                    margin: EdgeInsets.symmetric(
                                        vertical: 20, horizontal: 5),
                                    child: myIcon,
                                  ),
                                ),
                                Container(
                                  height: 120,
                                  width: 300,
                                  decoration: BoxDecoration(),
                                ),
                                Container(
                                  alignment: Alignment.bottomLeft,
                                  margin: EdgeInsets.only(top: 3, left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Nusa Penida',
                                        style: TextStyle(
                                            fontFamily: 'RobotoSlab',
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        children: [
                                          Icon(Icons.location_on_sharp,
                                              color: Colors.white, size: 15),
                                          Text('Bali, Indonesia',
                                              style: TextStyle(
                                                  fontFamily: 'RobotoSlab',
                                                  color: Colors.white)),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) {
                          return Demo();
                        },
                      ));
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 20),
                      child: Stack(
                        alignment: Alignment.bottomCenter,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(25),
                            child: Image.asset('assets/images/Raja Ampat.jpg',
                                fit: BoxFit.fill, width: 200, height: 300),
                          ),
                          Container(
                            width: 180,
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              children: [
                                InkWell(
                                  onTap: () {
                                    if (isTap) {
                                      setState(() {
                                        myIcon = Icon(Icons.favorite_border);
                                        isTap = !isTap;
                                      });
                                    } else {
                                      isTap = !isTap;
                                      setState(() {
                                        myIcon = Icon(
                                          Icons.favorite_rounded,
                                          color: Colors.red,
                                        );
                                      });
                                    }
                                  },
                                  child: Container(
                                    alignment: Alignment.topRight,
                                    margin: EdgeInsets.symmetric(
                                        vertical: 20, horizontal: 5),
                                    child: myIcon,
                                  ),
                                ),
                                Container(
                                  height: 120,
                                  width: 300,
                                  decoration: BoxDecoration(),
                                ),
                                Container(
                                  alignment: Alignment.bottomLeft,
                                  margin: EdgeInsets.only(top: 3, left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Raja Ampat',
                                        style: TextStyle(
                                            fontFamily: 'RobotoSlab',
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        children: [
                                          Icon(Icons.location_on_sharp,
                                              color: Colors.white, size: 15),
                                          Text('Lombok, Indonesia',
                                              style: TextStyle(
                                                  fontFamily: 'RobotoSlab',
                                                  color: Colors.white)),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
              alignment: Alignment.topLeft,
              child: Text(
                'Popular Catagories',
                style: TextStyle(
                    fontFamily: 'RobotoSlab',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 17),
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 3),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.blueAccent,
                          child: Image.asset('assets/images/Hotel.png',
                              height: 29, width: 30),
                        ),
                        CircleAvatar(
                          backgroundColor: Colors.blueAccent,
                          child: Image.asset('assets/images/Events.png',
                              height: 29, width: 30),
                        ),
                        CircleAvatar(
                          backgroundColor: Colors.blueAccent,
                          child: Image.asset('assets/images/Camping.jpg',
                              height: 30, width: 30),
                        ),
                        CircleAvatar(
                          backgroundColor: Colors.blueAccent,
                          child: Image.asset('assets/images/Trips.jpg',
                              height: 29, width: 30),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Hotel', style: TextStyle(fontFamily: 'RobotoSlab',fontSize: 15)),
                        Text('Events',style: TextStyle(fontFamily: 'RobotoSlab',fontSize: 15)),
                        Text('Camping',style: TextStyle(fontFamily: 'RobotoSlab',fontSize: 15)),
                        Text('Trips',style: TextStyle(fontFamily: 'RobotoSlab',fontSize: 15)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

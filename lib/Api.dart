import 'dart:convert';
import 'package:flutter/material.dart';
import 'Screen_3_1.dart';
import 'Screen_3_2.dart';
import 'catagorie.dart';
import 'demo.dart';
import 'drawer.dart';
import 'package:http/http.dart' as http;

class Screen2_Api extends StatefulWidget {
  const Screen2_Api({Key? key}) : super(key: key);

  @override
  State<Screen2_Api> createState() => _Screen2State();
}

class _Screen2State extends State<Screen2_Api> {
  bool isClicked = true;
  Color mycolor1 = Colors.grey;
  bool isTap = true;
  Icon myIcon = Icon(Icons.favorite_border_rounded);
  Color mycolor2 = Colors.black;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 30, left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) {
                            return Drawer1(child: ListView());
                          },
                        ));
                      },
                      icon: Icon(
                        Icons.menu,
                        color: Colors.black,
                        size: 30,
                      )),
                  CircleAvatar(
                    backgroundImage: AssetImage('assets/images/pngegg.png'),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40, left: 25, bottom: 15),
              alignment: Alignment.topLeft,
              child: Text(
                "Let's enjoy",
                style: TextStyle(fontSize: 25,fontFamily: 'RobotoSlab',),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 25,bottom: 10),
              alignment: Alignment.topLeft,
              child: Text(
                "Your Vacation!",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold,fontFamily: 'RobotoSlab',),
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
              decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.1),
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: Colors.white),
              ),
              child: TextField(style: TextStyle(fontFamily: 'RobotoSlab'),
                decoration: InputDecoration(
                  hintText: "Search Your Trip",
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.black,
                  ),
                  suffixIcon: CircleAvatar(
                      backgroundColor: Colors.cyan,
                      child: Image.asset(
                        'assets/images/filter-icon.png',
                        color: Colors.white,
                        height: 25,
                      )),
                  border: InputBorder.none,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 25,right: 25, bottom: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      if (isClicked) {
                        setState(() {
                          mycolor1 = Colors.cyan;
                          isClicked = !isClicked;
                        });
                      } else {
                        setState(() {
                          mycolor1 = Colors.grey;
                          isClicked = !isClicked;
                        });
                      }
                    },
                    child: Text(
                      'All',
                      style: TextStyle(color: mycolor1, fontSize: 13,fontFamily: 'RobotoSlab',),
                    ),
                  ),
                  Text(
                    'New',
                    style: TextStyle(color: Colors.grey, fontSize: 13,fontFamily: 'RobotoSlab',),
                  ),
                  Text(
                    'Most Viewed',
                    style: TextStyle(color: Colors.grey, fontSize: 13,fontFamily: 'RobotoSlab',),
                  ),
                  Text(
                    'Recommended',
                    style: TextStyle(color: Colors.grey, fontSize: 13,fontFamily: 'RobotoSlab',),
                  ),
                ],
              ),
            ),
            Container(
              height: 250,
              margin: EdgeInsets.only(left: 10),
              child: FutureBuilder<http.Response>(
                future: getApiData(),
                builder: (context, snapshot) {
                  if(snapshot.hasData){
                    return ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: jsonDecode(snapshot.data!.body.toString()).length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            if(index%2==0)
                            {
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                                  return Screen_3_2();},));
                            }
                            else
                            {
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                                return Screen_3_1();},));
                            }
                          },
                          child: Container(margin: EdgeInsets.symmetric(horizontal: 10),
                            child: Stack(
                              children: [
                                ClipRRect(borderRadius: BorderRadius.circular(25),child: Image.network(jsonDecode(snapshot.data!.body.toString())[index]["Image"].toString(),fit: BoxFit.fill, width: 200, height: 250)),
                                Container(
                                  width: 180,
                                  margin: EdgeInsets.symmetric(horizontal: 10),
                                  child: Column(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          if (isTap) {
                                            setState(() {
                                              myIcon = Icon(Icons.favorite_border);
                                              isTap = !isTap;
                                            });
                                          } else {
                                            isTap = !isTap;
                                            setState(() {
                                              myIcon = Icon(
                                                Icons.favorite_rounded,
                                                color: Colors.red,
                                              );
                                            });
                                          }
                                        },
                                        child: Container(
                                          alignment: Alignment.topRight,
                                          margin: EdgeInsets.symmetric(
                                              vertical: 20, horizontal: 5),
                                          child: myIcon,
                                        ),
                                      ),
                                      Container(
                                        height: 120,
                                        width: 300,
                                        decoration: BoxDecoration(),
                                      ),
                                      Container(
                                        alignment: Alignment.bottomLeft,
                                        margin: EdgeInsets.only(top: 3, left: 10),
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(jsonDecode(snapshot.data!.body.toString())[index]["FacultyName"].toString(),
                                              style: TextStyle(
                                                  fontFamily: 'RobotoSlab',
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Row(
                                              children: [
                                                Icon(Icons.location_on_sharp,
                                                    color: Colors.white, size: 15),
                                                Text(jsonDecode(snapshot.data!.body.toString())[index]["Department"].toString(),
                                                    style: TextStyle(
                                                        fontFamily: 'RobotoSlab',
                                                        color: Colors.white)),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },

                    );
                  }
                  else
                  {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 20,left: 20,bottom: 10),
              alignment: Alignment.topLeft,
              child: Text(
                'Popular Catagories',
                style: TextStyle(
                    fontWeight: FontWeight.bold,fontFamily: 'RobotoSlab',
                    color: Colors.black,
                    fontSize: 15),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 20, top: 10,right: 20),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 3),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                              return catagories();
                            },));
                          },
                          child: CircleAvatar(
                            backgroundColor: Colors.cyan,
                            child: Image.asset('assets/images/Hotel.png',
                                height: 29, width: 30),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                              return catagories();
                            },));
                          },
                          child: CircleAvatar(
                            backgroundColor: Colors.cyan,
                            child: Image.asset('assets/images/Events.png',
                                height: 29, width: 30),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                              return catagories();
                            },));
                          },
                          child: CircleAvatar(
                            backgroundColor: Colors.cyan,
                            child: Image.asset('assets/images/Camping.jpg',
                                height: 30, width: 30),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                              return catagories();
                            },));
                          },
                          child: CircleAvatar(
                            backgroundColor: Colors.cyan,
                            child: Image.asset('assets/images/Trips.jpg',
                                height: 29, width: 30),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Hotel', style: TextStyle(color: Colors.grey,fontSize: 12,fontFamily: 'RobotoSlab',)),
                        Text('Events', style: TextStyle(color: Colors.grey,fontFamily: 'RobotoSlab',fontSize: 12)),
                        Text('Camping', style: TextStyle(color: Colors.grey,fontFamily: 'RobotoSlab',fontSize: 12)),
                        Text('Trips', style: TextStyle(color: Colors.grey,fontFamily: 'RobotoSlab',fontSize: 12)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  Future<http.Response> getApiData() async {
    var response = await http
        .get(Uri.parse('https://6316f09acb0d40bc41475e91.mockapi.io/Faculty'));
    print(response.body.toString());
    return response;
  }
}

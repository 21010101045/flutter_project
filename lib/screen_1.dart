import 'package:flutter/material.dart';
import 'package:project_1/Api.dart';
import 'screen_2.dart';

class Screen1 extends StatelessWidget {
  const Screen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            flex: 2,
            child: Container(
              child: Stack(
                fit: StackFit.expand,
                children: [
                  Image.asset(
                    'assets/images/new.jpg',
                    fit: BoxFit.fill,
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          margin:
                              EdgeInsets.only(top: 70, left: 40),
                          child: Row(
                            children: [
                              Image.asset('assets/images/letter-c _2.png',
                                  height: 30, width: 25),
                              Container(
                                child: Text(
                                  'ravel',
                                  style: TextStyle(
                                    fontSize: 25,
                                    fontFamily: 'RobotoSlab',
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 30,left: 40),
                          alignment: Alignment.topLeft,
                          child: Text(
                            'Enjoy every good moment',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                              fontFamily: 'RobotoSlab',
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: 40, vertical: 20),
                          alignment: Alignment.topLeft,
                          child: Text(
                            'Keep the wolrd adventurous forever A way to explore our beautiful planet',
                            style: TextStyle(
                              fontSize: 12,
                              fontFamily: 'RobotoSlab',
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 30),
                          alignment: Alignment.topLeft,
                          child: TextButton(
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return Screen2_Api();
                                  },
                                ),
                              );
                            },
                            child: Container(
                              width: 100,
                              height: 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Colors.deepOrange,
                              ),
                              child: Center(
                                child: Text(
                                  'Next',
                                  style: TextStyle(
                                      fontFamily: 'RobotoSlab',
                                      color: Colors.white,
                                      fontSize: 16),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

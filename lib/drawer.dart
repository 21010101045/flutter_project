import 'package:flutter/material.dart';

class Drawer1 extends StatelessWidget {
  const Drawer1({Key? key, required ListView child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
      ),
      body: Center(
          child: Text(
        "Drawer open",
        style: TextStyle(color: Colors.black, fontSize: 40),
      )),
    );
  }
}




